package ru.nlmk.study.mapper;

import ru.nlmk.study.dto.UserDTO;
import ru.nlmk.study.model.User;

import java.time.LocalDate;

public class UserMapper {

    public static User fromDto(UserDTO userDTO){
        if(!userDTO.getBirthDate().isEmpty()) {
            return User.builder()
                    .id(userDTO.getId())
                    .firstName(userDTO.getFirstName())
                    .lastName(userDTO.getLastName())
                    .birthDate(LocalDate.parse(userDTO.getBirthDate()))
                    .build();
        }
        return User.builder()
                .id(userDTO.getId())
                .firstName(userDTO.getFirstName())
                .lastName(userDTO.getLastName())
                .build();
    }

    public static UserDTO toDto(User user){
        return UserDTO.builder()
                .id(user.getId())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .birthDate(user.getBirthDate().toString())
                .build();
    }
    
}
