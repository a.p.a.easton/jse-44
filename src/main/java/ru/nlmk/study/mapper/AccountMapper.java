package ru.nlmk.study.mapper;

import ru.nlmk.study.dto.AccountDTO;
import ru.nlmk.study.model.Account;

public class AccountMapper {

    public static Account fromDto(AccountDTO accountDTO){
        return Account.builder()
                .customerId(accountDTO.getCustomerId())
                .accountNumber(accountDTO.getAccountNumber())
                .ballance(accountDTO.getBallance())
                .build();
    }

    public static AccountDTO toDto(Account account){
        return AccountDTO.builder()
                .customerId(account.getCustomerId())
                .accountNumber(account.getAccountNumber())
                .ballance(account.getBallance())
                .build();
    }
    
}
