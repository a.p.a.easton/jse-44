package ru.nlmk.study.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccountDTO {

    private static final Long serialVersionUID = 1L;

    private int customerId;
    private String accountNumber;
    private Long ballance;

}
