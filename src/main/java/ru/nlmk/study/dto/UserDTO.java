package ru.nlmk.study.dto;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {

    private static final Long serialVersionUID = 1L;

    @NonNull
    private int id;
    private String firstName;
    private String lastName;
    private String birthDate;

}
