package ru.nlmk.study.repository;

import org.apache.ibatis.session.SqlSession;
import ru.nlmk.study.model.User;
import ru.nlmk.study.repository.datasource.MyBatisUtil;
import ru.nlmk.study.repository.datasource.mapper.CustomerMBMapper;

import java.util.List;

public class UserRepository {

    public Integer create(User user) {
       try(SqlSession sqlSession = MyBatisUtil.getSqlSession()){
           CustomerMBMapper customerMBMapper = sqlSession.getMapper(CustomerMBMapper.class);
           Integer result = customerMBMapper.create(user);
           sqlSession.commit();
           return result;
       }
    }

    public void update(User user) {
        try(SqlSession sqlSession = MyBatisUtil.getSqlSession()){
            CustomerMBMapper customerMBMapper = sqlSession.getMapper(CustomerMBMapper.class);
            customerMBMapper.updateUser(user);
            sqlSession.commit();
        }
    }

    public void delete(Long id) {
        try(SqlSession sqlSession = MyBatisUtil.getSqlSession()){
            CustomerMBMapper customerMBMapper = sqlSession.getMapper(CustomerMBMapper.class);
            customerMBMapper.deleteUserById(id);
            sqlSession.commit();
        }
    }

    public List<User> findUsers(User user){
        try(SqlSession sqlSession = MyBatisUtil.getSqlSession()){
            CustomerMBMapper customerMBMapper = sqlSession.getMapper(CustomerMBMapper.class);
            List<User> users = customerMBMapper.findUsers(user);
            sqlSession.commit();
            return users;
        }
    }
}
