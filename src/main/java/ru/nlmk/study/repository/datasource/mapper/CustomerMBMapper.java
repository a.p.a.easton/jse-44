package ru.nlmk.study.repository.datasource.mapper;

import org.apache.ibatis.annotations.*;
import ru.nlmk.study.model.User;

import java.util.List;

public interface CustomerMBMapper {
    @Insert("insert into customer(first_name, last_name, birth_date) values (#{firstName}, #{lastName}, #{birthDate, typeHandler=org.apache.ibatis.type.LocalDateTypeHandler})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    Integer create(User user);

    @Update("update customer set first_name = #{firstName}, last_name = #{lastName}, birth_date = #{birthDate, typeHandler=org.apache.ibatis.type.LocalDateTypeHandler} where id = #{id}")
    void updateUser(User user);

    @Delete("delete from customer where id = #{id}")
    void deleteUserById(Long id);

    @Select({"<script>",
            "select * from customer",
            "  <where>",
            "    <if test='firstName != \"\"'>first_name=#{firstName}</if>",
            "    <if test='lastName != \"\"'>and last_name=#{lastName}</if>",
            "    <if test='birthDate != null'>and birth_date=#{birthDate}</if>",
            "  </where>",
            "</script>"})
    @Results({
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "birthDate", column = "birth_date")
    })
    List<User> findUsers(User user);
}
