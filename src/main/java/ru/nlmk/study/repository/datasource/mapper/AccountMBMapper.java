package ru.nlmk.study.repository.datasource.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Update;
import ru.nlmk.study.model.Account;

public interface AccountMBMapper {
    @Insert("insert into account(customer_id, account_number, ballance) values (#{customerId}, #{accountNumber}, #{ballance})")
    @Options(useGeneratedKeys = true, keyProperty = "customerId")
    Integer create(Account account);

    @Update("update account set customer_id = #{customerId}, account_number = #{accountNumber}, ballance = #{ballance} where customer_id = #{customerId}")
    void updateUser(Account account);

    @Delete("delete from account where customer_id = #{customerId}")
    void deleteUserById(Long id);
}
