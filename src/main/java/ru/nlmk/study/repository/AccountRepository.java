package ru.nlmk.study.repository;

import org.apache.ibatis.session.SqlSession;
import ru.nlmk.study.model.Account;
import ru.nlmk.study.repository.datasource.MyBatisUtil;
import ru.nlmk.study.repository.datasource.mapper.AccountMBMapper;

public class AccountRepository {

    public Integer create(Account account) {
        try(SqlSession sqlSession = MyBatisUtil.getSqlSession()){
            AccountMBMapper accountMBMapper = sqlSession.getMapper(AccountMBMapper.class);
            Integer result = accountMBMapper.create(account);
            sqlSession.commit();
            return result;
        }
    }

    public void update(Account account) {
        try(SqlSession sqlSession = MyBatisUtil.getSqlSession()){
            AccountMBMapper accountMBMapper = sqlSession.getMapper(AccountMBMapper.class);
            accountMBMapper.updateUser(account);
            sqlSession.commit();
        }
    }

    public void delete(Long id) {
        try(SqlSession sqlSession = MyBatisUtil.getSqlSession()){
            AccountMBMapper accountMBMapper = sqlSession.getMapper(AccountMBMapper.class);
            accountMBMapper.deleteUserById(id);
            sqlSession.commit();
        }
    }

}
