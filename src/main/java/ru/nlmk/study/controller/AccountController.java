package ru.nlmk.study.controller;

import ru.nlmk.study.dto.AccountDTO;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface AccountController {

    @WebMethod
    Integer create(AccountDTO accountDTO);

    @WebMethod
    void update(AccountDTO accountDTO);

    @WebMethod
    void delete(Long id);

}
