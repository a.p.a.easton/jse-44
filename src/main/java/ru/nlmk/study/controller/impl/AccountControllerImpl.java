package ru.nlmk.study.controller.impl;

import ru.nlmk.study.controller.AccountController;
import ru.nlmk.study.dto.AccountDTO;
import ru.nlmk.study.service.AccountService;

import javax.jws.WebService;

@WebService(endpointInterface = "ru.nlmk.study.controller.AccountController")
public class AccountControllerImpl implements AccountController {

    AccountService accountService;

    public AccountControllerImpl() {
    }

    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    @Override
    public Integer create(AccountDTO accountDTO) {
        return accountService.create(accountDTO);
    }

    @Override
    public void update(AccountDTO accountDTO) {
        accountService.update(accountDTO);
    }

    @Override
    public void delete(Long id) {
        accountService.delete(id);
    }
}
