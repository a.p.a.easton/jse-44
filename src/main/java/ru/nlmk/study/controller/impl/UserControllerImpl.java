package ru.nlmk.study.controller.impl;

import ru.nlmk.study.controller.UserController;
import ru.nlmk.study.dto.UserDTO;
import ru.nlmk.study.dto.UserResponseDTO;
import ru.nlmk.study.service.UserService;

import javax.jws.WebService;

@WebService(endpointInterface = "ru.nlmk.study.controller.UserController")
public class UserControllerImpl implements UserController {

    UserService userService;

    public UserControllerImpl() {
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public Integer create(UserDTO userDTO) {
        return userService.create(userDTO);
    }

    @Override
    public void update(UserDTO userDTO) {
        userService.update(userDTO);
    }

    @Override
    public void delete(Long id) {
        userService.delete(id);
    }

    @Override
    public UserResponseDTO findUsers(UserDTO userDTO) {
        return userService.findUsers(userDTO);
    }
}
