package ru.nlmk.study.controller;

import ru.nlmk.study.dto.UserDTO;
import ru.nlmk.study.dto.UserResponseDTO;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface UserController {

    @WebMethod
    Integer create(UserDTO userDTO);

    @WebMethod
    void update(UserDTO userDTO);

    @WebMethod
    void delete(Long id);

    @WebMethod
    UserResponseDTO findUsers(UserDTO userDTO);
}
