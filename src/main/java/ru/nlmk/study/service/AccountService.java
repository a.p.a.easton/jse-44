package ru.nlmk.study.service;

import ru.nlmk.study.dto.AccountDTO;
import ru.nlmk.study.mapper.AccountMapper;
import ru.nlmk.study.model.Account;
import ru.nlmk.study.repository.AccountRepository;

public class AccountService {

    AccountRepository accountRepository;

    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }


    public Integer create(AccountDTO accountDTO) {
        Account account = AccountMapper.fromDto(accountDTO);
        return accountRepository.create(account);
    }

    public void update(AccountDTO accountDTO) {
        Account account = AccountMapper.fromDto(accountDTO);
        accountRepository.update(account);
    }

    public void delete(Long id) {
        accountRepository.delete(id);
    }
}
