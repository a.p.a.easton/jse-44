package ru.nlmk.study.service;

import ru.nlmk.study.dto.UserDTO;
import ru.nlmk.study.dto.UserResponseDTO;
import ru.nlmk.study.mapper.UserMapper;
import ru.nlmk.study.model.User;
import ru.nlmk.study.repository.UserRepository;

import java.util.List;

public class UserService {

    UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Integer create(UserDTO userDTO) {
        User user = UserMapper.fromDto(userDTO);
        return userRepository.create(user);
    }

    public void update(UserDTO userDTO) {
        User user = UserMapper.fromDto(userDTO);
        userRepository.update(user);
    }

    public void delete(Long id) {
        userRepository.delete(id);
    }

    public UserResponseDTO findUsers(UserDTO userDTO){
        User user = UserMapper.fromDto(userDTO);
        List<User> users = userRepository.findUsers(user);
        users.forEach(System.out::println);
        UserDTO[] usersDTO = users.stream().map(x -> UserMapper.toDto(x)).toArray(UserDTO[]::new);
        return UserResponseDTO.builder()
                .usersDTO(usersDTO)
                .build();
    }
}
