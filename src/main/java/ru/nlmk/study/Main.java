package ru.nlmk.study;

import ru.nlmk.study.controller.impl.AccountControllerImpl;
import ru.nlmk.study.controller.impl.UserControllerImpl;
import ru.nlmk.study.repository.AccountRepository;
import ru.nlmk.study.repository.UserRepository;
import ru.nlmk.study.service.AccountService;
import ru.nlmk.study.service.UserService;

import javax.xml.ws.Endpoint;

public class Main {
    public static void main(String[] args) {
        UserControllerImpl userController = new UserControllerImpl();
        userController.setUserService(new UserService(new UserRepository()));
        Endpoint.publish("http://localhost:8888/ws/user", userController);

        AccountControllerImpl accountController = new AccountControllerImpl();
        accountController.setAccountService(new AccountService(new AccountRepository()));
        Endpoint.publish("http://localhost:8889/ws/account", accountController);
    }
}
